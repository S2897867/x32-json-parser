let fs = require('fs');
console.log('Done');
process.argv.forEach(function (val, index, array) {//print out each flag
    if (index > 1){
        console.log('found flag: '+val);
    }
});
var controllerName = process.argv[2];//get controller name
controllerName = controllerName.charAt(0).toUpperCase() + controllerName.slice(1);//cap first letter
if (controllerName === undefined){ //die if no file name
    console.log('Error: controller name required');
    console.log('Controller creation failed');
    process.exit(0);
}
console.log('Creating Controller "'+controllerName+'"');
fs.readFile('framework/templates/controller.js', 'utf8', function (err,data) {
    if (err) {
        console.log('Error: ' + err);
        console.log('Failed: ' + err);
        process.exit(1);
    }
    var newController = data.replace('##className##',controllerName+"Controller");//replace name
    newController = newController.replace('##classNExport##',controllerName+"Controller");//replace export 
        fs.writeFile("controllers/" + controllerName + "Controller.js", newController, function(err) {
        if(err) {
            console.log('Error: ' + err);
            console.log('Failed: ' + err);
            process.exit(1);
        }
    console.log('Created Controller "'+controllerName+'"');
    console.log('Done');
    });
});
