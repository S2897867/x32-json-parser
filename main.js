//default modules
    const electron = require('electron');
    // Module to control application life.
    const app = electron.app;
    // Module to create native browser window.
    const BrowserWindow = electron.BrowserWindow;
    const path = require('path');
    const url = require('url');
//custom moduels
    const ipcMain = require('electron').ipcMain;
/*
set the aplications mode
1 = debug
0 = production
*/
    /* ------ default var  ------ */ /* ------ setting ------ */ /* ------ description ------ */
    let lightMode =                             1;              //  1 OR -d = debug | 0 OR -p = production |
    const defaultWinWidth =                     1280;           //  windows default min width
    const defaultWinHeight =                    720;            //  windows default min height
    let disableCLog =                         false;          //  disables console.log() function
//read in comand line args
    process.argv.forEach(function (val, index, array) {
        console.log('found flag: '+val);
        switch (val){
            case "d" :lightMode = 1; break;
            case "p": lightMode = 0; break;
            // case "c": disableCLog = true; break;
        }
    });
//disables
    //console.log



// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let backgroundWindow;

function createWindow () {
    //create windows
    if (lightMode == 1){//debug
         mainWindow = new BrowserWindow({width: defaultWinWidth , height: defaultWinHeight});
         mainWindow.webContents.openDevTools();
         mainWindow.setMinimumSize(defaultWinWidth , defaultWinHeight);

         backgroundWindow = new BrowserWindow({width: defaultWinWidth , height: defaultWinHeight});
         backgroundWindow.webContents.openDevTools();
         console.log('Running in debug mode.');
    }else if (lightMode == 0){//release
        mainWindow = new BrowserWindow({width: defaultWinWidth , height: defaultWinHeight});
        mainWindow.setMinimumSize(defaultWinWidth , defaultWinHeight);

        backgroundWindow = new BrowserWindow({ show: false });
         console.log('Running in production mode.');
    }
  // and load the index.html of the app.
  mainWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'views/index.html'),
    protocol: 'file:',
    slashes: true
  }))
  backgroundWindow.loadURL(url.format({
    pathname: path.join(__dirname, 'views/background.html'),
    protocol: 'file:',
    slashes: true
  }))


  // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        backgroundWindow.close();
    });
    backgroundWindow.on('closed', function () {
        mainWindow.close();
    });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
})

app.on('activate', function () {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

//route passer
ipcMain.on('route', function(event, arg) {
  console.log(arg);  // prints "ping"
  event.sender.send('route', arg);
  backgroundWindow.webContents.send('route', arg);
});
//action return;
ipcMain.on('action', function(event, arg) {
  console.log(arg);  // prints "ping"
  event.sender.send('action', arg);
  mainWindow.webContents.send('action', arg);
});
