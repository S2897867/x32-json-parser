//jQury plugin
(function($) {

    //gets all inputs off forms
    $.fn.inputGet = function(moreData = {}) {
        //grab data from form
        id = this.attr('id');
        inputs =  $('#'+id+' :input');
        var formData = {};
        for ( i = 0; i < inputs.length; i++){
            //get each elelments id and store the value in a key list
            inputName =  $(inputs[i]).attr('id');
            if (!$(inputs[i]).is(':checkbox')){
                if (inputs[i].id != ""){ //remove random inputs with ""as field name
                    formData[inputs[i].id] = php.strip_tags( inputs[i].value);
                    // if (formData[inputs[i].id] == ""){ //set blank inputs to "n/a";
                    //     formData[inputs[i].id] = "n/a";
                    // }
                }
            }else{
                formData[inputs[i].id] = inputs[i].checked;
            }
        }
        //merge data
        fomrData = $.extend(formData, moreData);
        return formData;
    };
    //locks the form
    $.fn.inputLock = function() {
        //disbale form inputs
        this.find(".input-field").each(function() {
            $(this).find("input").each(function() {
                $(this).attr('disabled', 'disabled');
                // console.log(this);
            });
        });
        //disable buttons
        this.find('.btn').each(function(){
            $(this).attr('disabled', 'disabled');
        });
        this.find('.modal-footer').each(function(){
            $(this).attr('disabled', 'disabled');
        });
    }
    //unlocks forms
    $.fn.inputUnLock = function() {
        //enable form inputs
        this.find(".input-field").each(function() {
            $(this).find("input").each(function() {
                $(this).removeAttr('disabled');
                console.log(this);
            });
        });
        //enable buttons
        this.find('.btn').each(function(){
            $(this).removeAttr('disabled');
        });
        this.find('.modal-footer').each(function(){
            $(this).removeAttr('disabled');
        });
    };
    //adds error to an input
    $.fn.inputError = function(id,error="Something Went Wrong.") {
        if (error !== false){
            var input = this.find(id);//find input witin forms
            input.addClass( 'invalid'); //set input to invalid
            input.next().attr('data-error',error);//find lable
        }
        //console.log(input);
    };
    //clears inputs in form
    $.fn.inputClear = function(){
        this.find(".input-field").each(function() {
            $(this).find("input").each(function() {
                $(this).val('');
                $(this).next().removeAttr('data-error');//find lable
                $(this).removeClass('invalid');
                console.log(this);
            });
        });
    };
    $.fn.inputSet = function(id,value){
        $(this).find(id).val(value);
        // this.find(".input-field").each(function() {
        //     $(this).find(id).each(function() {
        //         $(this).val(value);
        //         console.log(this);
        //     });
        // });
    };



}(jQuery));
