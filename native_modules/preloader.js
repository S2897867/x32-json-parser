//jQury plugin
(function($) {

    //adds preloader
    $.fn.preLock = function(moreData = {}) {
        this.html(`
        <div class="preloader-wrapper big active preCenter">
            <div class="spinner-layer spinner-blue-only">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                    </div><div class="gap-patch">
                    <div class="circle"></div>
                    </div><div class="circle-clipper right">
                    <div class="circle"></div>
                </div>
            </div>
        </div>
            `);
    };
    //removes preloader
    $.fn.preUnLock = function(moreData = {}) {
        this.html(` `);
    };



}(jQuery));
