class Action {
    constructor() {
        //parent run function when child asks
        ipcRenderer.on('action', function(event, arg) {
            //console.log(arg);
            switch (arg.task){
                case 'log': console.log(arg.data); break;
                case 'html': job.html(arg.data); break;
                case 'clear': job.clear(arg.data); break;
                case 'append': job.append(arg.data); break;
                case 'preLock': job.preLock(arg.data);  break;
                case 'preUnLock': job.preUnLock(arg.data);  break;
                case 'inputLock': job.inputLock(arg.data);  break;
                case 'inputUnLock': job.inputUnLock(arg.data);  break;
                case 'inputError': job.inputError(arg.data);  break;
                case 'inputClear': job.inputClear(arg.data);  break;
                case 'inputSet': job.inputSet(arg.data);  break;
            }
        });
    }

    //listen to ipc channel
    static listen (){
        ipcRenderer.on('action', function(event, arg) {
            console.log('action'+" '"+arg.task+"' called with:");
            console.log(arg.data); // prints "pong"
        });
    }
    //tell parent to do something
    static do (task,data){
        ipcRenderer.send('action', {
            task:task,
            data:data
        });
    }
}

class job {
    static html (data){
        $(data.id).html(data.html);
    }
    static append (data){
        $(data.id).append(data.html);
    }
    static clear (data){
        $(data.id).html('');
    }
    static preLock (data){
            $(data).preLock();
    }
    static preUnLock (data){
            $(data).preUnLock();
    }
    static inputLock (data){
            $(data).inputLock();
    }
    static inputUnLock (data){
            $(data).inputUnLock();
    }
    static inputError (data){
            $(data.form).inputError(data.id,data.error);
    }
    static inputClear (data){
            $(data).inputClear();
    }
    static inputSet (data){
            $(data.form).inputSet(data.id,data.val);
    }
}


class Route {
    //the constructor registors tasks for the background process to do
    constructor (callback){
        ipcRenderer.on('route', function(event, arg) {
            if (arg.task == 'log'){
                console.log(arg.data);
            }else{
                callback(arg);
            }
        });
    }
    //listen to ipc channel
    static listen (){
        ipcRenderer.on('route', function(event, arg) {
            console.log('route'+" '"+arg.task+"' called with:");
            console.log(arg.data); // prints "pong"
        });
    }

    //send message to child
    static task(task,data=""){
        ipcRenderer.send('route', {
            task:task,
            data:data
        });
    }

}
