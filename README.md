# electrolight electron-quick-start

**This is a custom preconfigured version of Electron**

This is a minimal Electron application based on the [Quick Start Guide](http://electron.atom.io/docs/tutorial/quick-start)..
Materialize CSs, jQuery, hammer and SqLite3 have been added to the default installation.


**Use this app along with the [Electron API Demos](http://electron.atom.io/#get-started) app for API code examples to help you get started.**

A basic Electron application needs just these files:

- `package.json` - Points to the app's main file and lists its details and dependencies.
- `main.js` - Starts the app and creates a browser window to render HTML. This is the app's **main process**.
- `index.html` - A web page to render. This is the app's **renderer process**.

You can learn more about each of these components within the [Quick Start Guide](http://electron.atom.io/docs/tutorial/quick-start).

## To Use

To clone and run this repository you'll need [Git](https://git-scm.com) and [Node.js](https://nodejs.org/en/download/) (which comes with [npm](http://npmjs.com)) installed on your computer. From your command line:


1. Clone this repository
> git clone https://S2897867@bitbucket.org/S2897867/electro-light.git
2. open folder
> cd  electron-base-sqlite3-mcss
3. Install dependencies
> npm install
4. Run the app
> npm start

Note: If you're using Linux Bash for Windows, [see this guide](https://www.howtogeek.com/261575/how-to-run-graphical-linux-desktop-applications-from-windows-10s-bash-shell/) or use `node` from the command prompt.

## SqLite Compile

Sqlite3 needs to be rebuilt with Electron headers or it won't run.
To rebuild native Node modules for Electron, follow these instructions 

1. install the package using 
> npm install packageName --save
2. add the rebuild tool to the package folder
> cd node_modules\packageName
>
> npm install --save-dev electron-rebuild
3. rebuild from source
> .\node_modules\\.bin\electron-rebuild.cmd --build-from-source


## Resources for Learning Electron

- [electron.atom.io/docs](http://electron.atom.io/docs) - all of Electron's documentation
- [electron.atom.io/community/#boilerplates](http://electron.atom.io/community/#boilerplates) - sample starter apps created by the community
- [electron/electron-quick-start](https://github.com/electron/electron-quick-start) - a very basic starter Electron app
- [electron/simple-samples](https://github.com/electron/simple-samples) - small applications with ideas for taking them further
- [electron/electron-api-demos](https://github.com/electron/electron-api-demos) - an Electron app that teaches you how to use Electron
- [hokein/electron-sample-apps](https://github.com/hokein/electron-sample-apps) - small demo apps for the various Electron APIs

## License

[CC0 1.0 (Public Domain)](LICENSE.md)
